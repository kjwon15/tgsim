import { Stack } from './stack.js';
import { Storage } from './storage.js';

const app = document.querySelector('#app');

const sounds = {};

const imageBox = document.createElement('div');
const backgrounds = document.createElement('div');

const selectBox = document.createElement('div');

const dialogBox = document.createElement('div');
const nameBox = document.createElement('div');
const msgBox = document.createElement('div');

function setup() {
    imageBox.classList.add('image-box');
    selectBox.classList.add('select-box');
    dialogBox.classList.add('dialog-box');

    backgrounds.classList.add('backgrounds');
    imageBox.appendChild(backgrounds);

    nameBox.classList.add('name-box');
    msgBox.classList.add('msg-box');
    dialogBox.appendChild(nameBox);
    dialogBox.appendChild(msgBox);

    app.appendChild(imageBox);
    app.appendChild(selectBox);
    app.appendChild(dialogBox);
    app.setAttribute('tabindex', 1);
}

export async function loadAndStart({toLoads, labels, commands}) {
    setup();

    let loadingProgress = document.createElement('p');
    const total = toLoads.images.size + toLoads.sounds.size;

    loadingProgress.innerText = `Loading ${total} resources`;

    app.prepend(loadingProgress);

    let promises = [];
    for (let imgSrc of toLoads.images) {
        let promise = new Promise(resolve => {
            let obj = new Image();
            obj.src = imgSrc;
            obj.onload = () => {
                loadingProgress.innerText += `\n${imgSrc}`;
                resolve();
            };
            obj.onerror = resolve; // If reject, game won't start.
        });
        promises.push(promise);
    }

    for (let audioSrc of toLoads.sounds) {
        let promise = new Promise(resolve => {
            let obj = new Audio(audioSrc);
            obj.onloadeddata = () => {
                sounds[audioSrc] = obj;
                loadingProgress.innerText += `\n${audioSrc}`;
                resolve();
            };
            obj.onerror = resolve;
        });
        promises.push(promise);
    }

    return await Promise.all(promises)
        .then(() => {
            window.storage = new Storage();
            app.focus();
            app.removeChild(loadingProgress);
            return commandLoop({ labels, commands });
        });
}

async function commandLoop({labels, commands}) {
    let pc = labels.start;
    const callStack = new Stack();

    const functions = {
        scene: scene,
        bg: setBackground,
        speak: speak,
        sound: playSound,
        show: showImg,
        if: doIf,
        menu: menu,
        // TODO: Don't use eval.
        eval: eval,
        jump: (arg) => { pc = arg; },
        return: () => {
            if (callStack.size) {
                pc = callStack.pop();
            } else {
                pc = labels.start;
            }
        }
    };

    while (commands[pc]) {
        const args = commands[pc].slice();
        const command = args.shift();
        let func = functions[command];
        if (func) {
            let result = await func(...args);
            if (command === 'menu') {
                pc = result;
            } else if (command === 'if') {
                if (result) {
                    pc = result;
                }
            }
        }
        pc += 1;
    }
}

async function speak(character, msg) {
    return new Promise(resolve => {
        try {
            let {name, color} = character;
            nameBox.innerText = name;
            nameBox.style.color = color;
        } catch(e) {
            nameBox.innerText = character;
        }
        msgBox.innerText = msg;

        if (character === null) {
            msgBox.classList.add('narration');
        } else {
            msgBox.classList.remove('narration');
        }

        app.onclick = resolve;
        app.onkeypress = event => {
            switch(event.code) {
            case 'Space':
            case 'Enter':
                resolve();
                break;
            }
        };
    });
}

function playSound(soundId) {
    let sound = sounds[soundId];
    if (!sound) return;

    sound.play();
}

async function scene(src, {duration=0, animation='fadeIn'}={}) {
    msgBox.innerText = '';
    msgBox.classList.remove('narration');
    nameBox.innerText = '';
    for (let img of imageBox.querySelectorAll('img')) {
        imageBox.removeChild(img);
    }
    await setBackground(src, {duration, animation});
}

function setBackground(src, {duration=0, animation='fadeIn'}={}) {
    return new Promise(resolve => {
        let previous = backgrounds.firstChild;
        let newElem = document.createElement('div');

        newElem.style.backgroundImage = `url('${src}')`;
        newElem.style.animationName = animation;
        newElem.style.animationDuration = `${duration}ms`;
        backgrounds.appendChild(newElem);

        setTimeout(() => {
            if (previous) {
                backgrounds.removeChild(previous);
            }
            newElem.style.animation = '';
            resolve();
        }, duration);
    });
}

function showImg(tag, src, position='center') {
    return new Promise(resolve => {
        let previous = imageBox.querySelector(`img[data-tag="${tag}"]`);
        if (previous) {
            imageBox.removeChild(previous);
        }

        let newElem = document.createElement('img');
        newElem.setAttribute('data-tag', tag);
        newElem.src = src;
        newElem.classList.add(position);
        imageBox.appendChild(newElem);
        resolve();
    });
}

function doIf(expression, positionElse) {
    const result = !!eval(expression);
    if (result === false) {
        return positionElse;
    }

    return;
}

function menu(...list) {
    return new Promise(resolve => {
        const handlers = [];
        for (let index = 0; index < list.length; index += 1) {
            const entry = list[index];
            let args = entry.slice();
            let msg = args.shift();
            let jump = args.shift();

            let button = document.createElement('button');
            if (index <= 10) {
                button.innerText = `${index + 1 % 10}. ${msg}`;
            } else {
                button.innerText = msg;
            }
            button.classList.add('choice');
            let handler = () => {
                clearMenu();
                resolve(jump);
            };
            handlers.push(handler);
            button.onclick = handler;

            selectBox.appendChild(button);
        }

        const handlerMap = {
            'Digit1': handlers[0],
            'Digit2': handlers[1],
            'Digit3': handlers[2],
            'Digit4': handlers[3],
            'Digit5': handlers[4],
            'Digit6': handlers[5],
            'Digit7': handlers[6],
            'Digit8': handlers[7],
            'Digit9': handlers[8],
            'Digit0': handlers[9],
        };

        app.onkeypress = (event) => {
            let handler = handlerMap[event.code];
            if (handler !== undefined) {
                return handler();
            }
        };
    });
}

function clearMenu() {
    while (selectBox.firstChild) {
        selectBox.removeChild(selectBox.firstChild);
    }
}
