# defind characters
define me = { name: 'Me', color: '#ff0000' }
define you = { name: 'You', color: '#00ff00' }

# assets
image miku1 normal = './images/miku1.png'
image miku2 normal = './images/miku2.png'
image bg bg1 = './images/livingroom.jpg'
image bg bg2 = './images/closet.jpg'

label start:
    scene bg bg1 { duration: 2000 }

    show miku1 normal at left
    me 'Hello?'
    show miku2 normal at right
    you 'Oh, hello'

    if storage.permanent.lastEnding:
        you "You have seen ending before"
    # label if-19-else
    else:
        you "Nice to meet you"
    # label if-19-end

    play sound "./sounds/noti-pop.mp3"
    show miku1 normal at left
    scene bg bg2 { animation: 'zoomIn', duration: 2000 }
    you 'Do you like me?'
menu:
    "Yes":
        jump good_ending
    "No":
        jump bad_ending

label good_ending:
    me 'Yes'
    you "I'm so happy!"
    "Good ending"
    $ storage.permanent.lastEnding = 'good';
    return

label bad_ending:
    me 'No'
    you "I'll kill you"
    "Bad ending"
    $ storage.permanent.lastEnding = 'bad';
    return
