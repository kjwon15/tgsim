import './style.scss';
import { loadAndStart } from './game.js';
import { loadScript } from './parser.js';

const scriptSrc = require('./script.rpy');

loadScript(scriptSrc)
    .then(loadAndStart);
