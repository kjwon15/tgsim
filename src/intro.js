import {choose, l, r, setBackground, state, toLoads} from './game.js';
import * as closet from './closet.js';
import livingroom from './images/livingroom.jpg';

toLoads.images.add(
    livingroom,
);

export function start() {
    setBackground(livingroom);
    l('안녕?');
    l('난 **정아름**이라고 해.');
    r('어..안녕?');
    l('사실 진짜 이름은 아니지만 뭐 아무튼.');
    l('이쪽 사람들은 이게 흔한 일이거든.');
    choose([{
        text: '알 게 뭐야',
        action: nah,
    }, {
        text: '왜 가짜 이름을 써?',
        action: descName,
    }]);
}

function nah() {
    r('알 게 뭐야');
    l('까칠하구만');
    l('이름도 안 알려주기는 좀 그렇잖아?');
    setTimeout(play1, 0);
}

function descName() {
    state.askedName = true;
    r('왜 실명을 놔두고 가짜 이름을 쓰는 거야?');
    r('설마 익명을 원한다거나..?');
    l('니 엄마가 니 이름을 _skxkjwemm_라고 지었다고 생각해 봐.');
    l('나도 저걸 뭐라고 읽는진 모르겠다만 아무튼 들을 때마다 불편하단 말이야.');
    setTimeout(play1, 0);
}

function play1() {
    l('아무튼 넌 이제부터 트젠들이 겪는 일들을 간접적으로 체험할거야.');
    l('일단 게임을 하기 전에 너에 대해서 좀 물어 볼 게 있어.');
    l('너의 성별은?');
    choose([{
        text: 'MtF 트랜스젠더',
        action: selectGender.bind(null, 'MtF'),
    }, {
        text: 'FtM 트랜스젠더',
        action: selectGender.bind(null, 'FtM'),

    }, {
        text: '왜 선택지가 이모양이야?',
        action: descGenderSelect.bind(null, 'wtf'),

    }, {
        text: '이걸 왜 물어봐?',
        action: descGenderSelect.bind(null, 'why'),
    }]);
}

function selectGender(gender) {
    state.gender = gender;
    switch(state.gender) {
    case 'MtF':
        r('MtF. 이걸로 할게');
        l('Male to Female이라.. 나랑 똑같네');
        l('아니 정확히는 다르지만 그냥 편하게 그렇다고 하자.');
        break;
    case 'FtM':
        r('FtM.. 이걸로 할게');
        l('Female to Male이라.. 멋지군.');
    }

    l('설마 뭔지도 모르고 그냥 누른 건 아니지?');
    if (!state.askedWhyGender) {
        l('갑자기 성별을 왜 물어보는지도 안 물어보는 걸 보니 그냥 관심이 없는 건가?');
    }

    l(`아무튼 넌 ${state.gender}의 입장에서 밖에 나가 볼거야.`);
    choose([{
        text: '간다',
        action: closet.start,
    }]);
}

function descGenderSelect(arg) {
    state.askedWhyGender = true;
    switch(arg) {
    case 'wtf':
        r('왜 선택지가 이딴 것밖에 없어?');
        r('그냥 평범하게 남자, 여자를 달란 말이야');
        l('...');
        l('너 같은 사람들 때문에 우리가 항상 고통 받는 거야.');
        l('내 심정을 좀 느껴보라는 좀 꼬인 마음이 들어가긴 했지만');
        break;
    case 'why':
        r('... 도대체 이런 걸 왜 물어보는거야?');
        l('좋은 질문이야.');
        l('사실 게임 하는데 성별따위 아무래도 상관 없는 거긴 하지.');
        l('근데 살다 보면 물어볼 필요도 없는 걸 꼭 물어보는 놈들이 있단 말이야?');
        l('대학생들이 과제라면서 설문조사지를 주는데 성별란이 꼭 있더라.');
        l('심지어는 학년을 적는 칸에 1~4학년밖에 없어. 5년제 학교도 있는데 말이야.');
        r('아.. 뭘 말하려는 지 알겠어.');
        break;
    }

    l('아무튼 넌 이 둘 중에서 성별을 하나 골라야 해.');
    l('원래 이거 말고도 무수히 많은 종류가 있기는 한데 일단은 넘어가자구.');

    choose([{
        text: 'MtF 트랜스젠더',
        action: selectGender.bind(null, 'MtF'),
    }, {
        text: 'FtM 트랜스젠더',
        action: selectGender.bind(null, 'FtM'),

    }]);
}
