import {clearDialog, choose, l, r, setBackground, state, toLoads} from './game.js';
import closet from './images/closet.jpg';

toLoads.images.add(closet);

export function start() {
    clearDialog();
    setBackground(closet);
    l('음.. 일단 옷을 입어야 하는데 그 전에 몇 가지 설명을 해 줄게.');
}
