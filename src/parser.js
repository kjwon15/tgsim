const JSON5 = require('json5');
import { Stack } from './stack.js';

const pattern = /^\s+|\w+|("|')(?:[^\1\\]|\\.)*?\1|{(?:("|')(?:[^\1\\]|\\.)*?\2|[^}])*}|\S/gm;
const quotePattern = /(["'])([^\1\\]|\\.)*\1/;
const commandPattern = /^(\s*)(\$)\s*(.*)$/m;
const whitePattern = /^\s*$/;

const images = require.context('.', true, /\.\/images\/.*\.(jpg|png)$/);
const sounds = require.context('.', true, /\.\/sounds\/.*\.(mp3|ogg)$/);


export function loadScript (scriptSrc) {
    return fetch(scriptSrc)
        .then(res => res.text())
        .then(parse);
}


function parse(content) {
    const intercode = [];
    const imageMap = {};
    const soundMap = {};
    const characters = {};
    if (whitePattern.test(content.replace(pattern, '')) !== true) {
        throw 'Script is invalid';
    }
    const lines = content.split('\n');
    const stack = new Stack();
    for (let lineNumber in lines) {
        let line = lines[lineNumber];
        // Remove comment
        line = line.replace(/^([^'"]|'(?:[^\\']|\\.)*'|"(?:[^\\"]|\\.)*")*(#.*)$/, '$1');

        let tokens;
        try {
            tokens = commandPattern.exec(line).slice(1);
        } catch(e) {
            tokens = line.match(pattern);
        }

        if (tokens === null) continue;

        let indentLevel = 0;
        if (whitePattern.test(tokens[0]) === true) {
            indentLevel = tokens.shift().replace('\t', ' '.repeat(8)).length;
        }

        if (tokens.length === 0) continue;

        while (stack.top && stack.top.indentLevel >= indentLevel) {
            const popped = stack.pop();
            if (popped.mode === 'choice') {
                intercode.push(['jump', stack.top.endIdentifier]);
            } else if (popped.mode === 'menu') {
                intercode.push(['label', popped.endIdentifier]);
            } else if (popped.mode === 'if') {
                if (tokens[0] === 'else' && popped.indentLevel === indentLevel) {
                    stack.push(popped);
                } else {
                    intercode.push(['label', popped.endIdentifier]);
                }
                break;
            }
        }

        switch(tokens[0]) {
        // Headers
        case 'define':
            defineCharacter(characters, tokens);
            break;

        case 'image':
            defineImage(imageMap, tokens);
            break;

        // Bodies
        case 'label':
            intercode.push(parseLabel(tokens));
            break;

        case 'scene':
            intercode.push(parseScene(imageMap, tokens));
            break;

        case 'show':
            intercode.push(parseShow(imageMap, tokens));
            break;

        case 'play':
            intercode.push(parsePlay(soundMap, tokens));
            break;

        // Specials
        case '$':
            intercode.push(['eval', tokens[1]]);
            break;

        case 'jump':
            intercode.push(['jump', tokens.slice(1)]);
            break;

        case 'return':
            intercode.push(['return']);
            break;

        case 'menu':
            intercode.push(parseMenu(lineNumber, indentLevel, stack, tokens));
            break;

        case 'if':
            intercode.push(parseIf(
                lineNumber, indentLevel, stack, /^\s*if\s+(.*):\s*$/.exec(line)[1]));
            break;

        case 'else':
            intercode.push(...parseElse(
                lineNumber, stack));
            break;

        default:
            if (stack.top && stack.top.mode === 'menu') {
                tokens = tokens.map(strip);
                if (tokens[1] !== ':') {
                    console.error(lineNumber, line);
                    break;
                }

                const labelName = `${stack.top.identifier}-choice-${tokens[0]}`;
                const code = ['label', labelName];
                intercode.push(code);
                stack.top.code.push([tokens[0], labelName]);
                stack.push({
                    'mode': 'choice',
                    identifier: labelName,
                    code: code,
                    indentLevel: indentLevel,
                });

            } else if (quotePattern.test(tokens[0])) {
                tokens = tokens.map(strip);
                // TODO: check first argument is character.
                if (tokens.length === 1) {
                    intercode.push(['speak', null, ...tokens]);
                } else {
                    intercode.push(['speak', ...tokens]);
                }
            } else if (tokens[0] in characters) {
                let name = tokens.shift();
                tokens = tokens.map(strip);
                intercode.push(['speak', characters[name], ...tokens]);
            } else {
                console.warn(lineNumber, tokens);
            }
            break;
        }
    }

    return afterProcess(intercode);
}

function afterProcess(commands) {
    const labels = {};
    const innerLabels = {};
    const toLoads = {
        images: new Set(),
        sounds: new Set([]),
    };

    const removeJump = (a, b) => {
        let lastCommand = a[a.length - 1];
        if (lastCommand && lastCommand[0] === 'jump' && b[0] === 'jump') {
            return a;
        }

        return a.concat([b]);
    };

    commands = commands.reduce(removeJump, []);

    for (let index in commands) {
        let args = commands[index];
        switch (args[0]) {
        case 'label':
            innerLabels[args[1]] = parseInt(index);
            if (!args[1].startsWith('$')) {
                labels[args[1]] = parseInt(index);
            }
            break;

        case 'scene':
        case 'bg':
            toLoads.images.add(args[1]);
            break;

        case 'show':
            toLoads.images.add(args[2]);
            break;

        case 'sound':
            toLoads.sounds.add(args[1]);
            break;
        }
    }

    for (let index in commands) {
        let args = commands[index];
        switch (args[0]) {
        case 'jump':
            commands[index] = ['jump', innerLabels[args[1]]];
            break;

        case 'menu':
            for (let i = 1; i < args.length; i += 1) {
                args[i][1] = innerLabels[args[i][1]];
            }
            break;

        case 'if':
            args[2] = innerLabels[args[2]];
            break;
        }

    }

    return {toLoads, labels, commands};
}

function defineCharacter(characters, tokens) {
    tokens.shift(); // "define"
    let name = tokens.shift();
    if (tokens.shift() !== '=') {
        throw `Invalid character define: ${tokens.join(' ')}`;
    }

    let kwargs = JSON5.parse(tokens.join(' '));
    characters[name] = kwargs;
}

function defineImage(imageMap, tokens) {
    let equalPosition = tokens.indexOf('=');
    tokens = tokens.map(strip);
    if (equalPosition === -1) {
        throw `Invalid image define: ${tokens.join(' ')}`;
    }

    let hash = tokens.slice(1, equalPosition).join(' ');
    let filename = tokens.slice(equalPosition + 1).join(' ');
    imageMap[hash] = images(`${filename}`);
}

function parseLabel(tokens) {

    if (tokens[tokens.length - 1] == ':') {
        return tokens.slice(0, tokens.length - 1);
    } else {
        throw 'Unexpected token';
    }
}

function parseScene(imageMap, tokens) {
    tokens.shift(); // "scene"
    let kwargs = tokens.pop();
    let hash = tokens.join(' ');
    let filename = imageMap[hash];

    return ['scene', filename, kwargs];
}

function parseShow(imageMap, tokens) {
    tokens.shift(); // "show"
    let position = null;
    if (tokens.length >= 4 && tokens[tokens.length - 2] === 'at') {
        position = tokens[tokens.length - 1];
        tokens = tokens.slice(0, tokens.length - 2);
    }
    let hash = tokens.join(' ');
    let filename = imageMap[hash];
    let tag = tokens.shift();


    return ['show', tag, filename, position];
}

function parsePlay(soundMap, tokens) {
    tokens.shift(); // play
    let type = tokens.shift();
    let key = strip(tokens.shift());
    let filename = sounds(key);
    // TODO: implement 'music', 'sound' type.
    return ['sound', filename];
}

function parseMenu(lineNumber, indentLevel, stack, tokens) {
    const code = ['menu'];
    stack.push({
        mode: 'menu',
        identifier: `$menu-${lineNumber}`,
        endIdentifier: `$menu-${lineNumber}-end`,
        code: code,
        choices: {},
        indentLevel: indentLevel,
    });
    return code;
}

function parseIf(lineNumber, indentLevel, stack, expression) {
    const identifier = `$if-${lineNumber}`;
    const endIdentifier = `${identifier}-end`;
    const code = ['if', expression, endIdentifier];
    stack.push({
        mode: 'if',
        identifier,
        endIdentifier,
        code,
        indentLevel,
    });
    return code;
}

function parseElse(lineNumber, stack) {
    const top = stack.top;
    const identifier = `${top.identifier}-else`;
    const codes = [
        ['jump', top.endIdentifier],
        ['label', identifier],
    ];

    // ['if', expression, 'if-{line}-end' -> 'if-line-else']
    top.code[2] = identifier;

    return codes;
}

function strip(token) {
    if (quotePattern.test(token)) {
        return token.replace(/^["']|["']$|\\(.)/g, '$1');
    }

    return token;
}
