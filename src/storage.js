export class Storage {
    constructor () {
        this._permanent = new Proxy(window.localStorage, {
            get: (target, name) => {
                return JSON.parse(target.getItem(name));
            },
            set: (target, name, value) => {
                target.setItem(name, JSON.stringify(value));
            },
        });

        this._temporary = new Proxy({}, {
            get: (target, name) => {
                return target[name];
            },
            set: (target, name, value) => {
                target[name] = value;
            }
        });
    }

    get permanent() {
        return this._permanent;
    }

    get temporary() {
        return this._temporary;
    }
}
