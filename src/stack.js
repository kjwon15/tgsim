export class Stack extends Array {

    constructor () {
        super();
    }

    get top() {
        return this[this.length - 1] || null;
    }

    get size() {
        return this.length;
    }
}
