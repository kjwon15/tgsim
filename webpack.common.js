const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        app: './src/entry.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[chunkhash].js',
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(png|svg|jpg|wav|mp3|ogg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                        }
                    }
                ],
            },
            {
                test: /\.rpy$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                    }
                }]
            }
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            title: 'TG simulator',
            inject: false,
            template: require('html-webpack-template'),
            appMountId: 'app',
            mobile: true,
            lang: 'ko',
            inlineManifestWebpackName: 'webpackManifest',
            links: [
                'https://fonts.googleapis.com/earlyaccess/notosanskr.css',
            ]
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common',
        }),
    ],
};
